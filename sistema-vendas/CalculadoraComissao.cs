﻿using System;

namespace com.br.k21.SistemaVendas
{
    public class CalculadoraComissao
    {
        public decimal Calcular(decimal p)
        {
            decimal comissao = 0.0M;
            if (p >= 0 && p <= 10000)
            {
                comissao = Decimal.Multiply(p, 0.05M);
            }
            else
            {
                comissao = Decimal.Multiply(p, 0.06M);
            }

            return Math.Floor(comissao * 100) / 100;
        }
    }
}