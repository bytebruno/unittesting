﻿using NUnit.Framework;

namespace com.br.k21.SistemaVendas.Testes
{
    public class TestesCalculadoraComissao
    {
        [Test]
        public void Venda_de_1000_retorna_comissao_de_50()
        {
            decimal comissaoEsperada = 50.0M;
            decimal totalVendas = 1000.0M;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(comissaoEsperada, comissao);
        }

        [Test]
        public void Venda_de_2000_retorna_comissao_de_100()
        {
            decimal totalVendas = 2000.0M;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(100.0, comissao);
        }

        [Test]
        public void Venda_de_10000_retorna_comissao_de_500()
        {
            decimal totalVendas = 10000M;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(500, comissao);
        }

        [Test]
        public void Venda_de_77_99_retorna_comissao_de_3_89()
        {
            decimal totalVendas = 77.99M;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(3.89, comissao);
        }

        [Test]
        public void Venda_de_10001_retorna_comissao_de_600_06()
        {
            decimal totalVendas = 10001M;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(600.06, comissao);
        }
    }
}